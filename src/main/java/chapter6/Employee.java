package chapter6;

public class Employee implements Comparable<Employee> {
    private String name;
    private int salary;

    @Override
    public int compareTo(Employee o) {
        return o.salary - this.salary;
    }
}