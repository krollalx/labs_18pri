package chapter6;

import java.util.List;

//public static <T extends Comparable<? super T>> void sort(List<T> list)
//
// turns into
// public static <Comparable> void sort(List<Comparable> list)

public class Collection {
//    private Comparable T;
    public static <T extends Comparable<? super T>> void sort(List<T> list) {

    }

}


// public static <T extends Object & Comparable<? super T>> T max(Collection<? extends T> coll)

// turns into
// public static <Object> Object max(Collection<Object> coll)