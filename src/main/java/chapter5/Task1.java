package chapter5;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Task1 {
    public ArrayList<Double> readValues(String filename) throws FileNotFoundException, NumberFormatException {
        if (filename == null || filename.isEmpty()) {
            throw new FileNotFoundException("Filename is missing");
        }

        ArrayList<Double> result = new ArrayList<>();
        Scanner in = new Scanner(new File(filename));
        while (in.hasNextDouble()) {
            result.add(in.nextDouble());
        }

        if (in.hasNext()) {
            throw new NumberFormatException("Input are not floating-point");
        }

        return result;
    }
}