package chapter5;

import java.io.FileNotFoundException;
import java.util.logging.Logger;

public class Task3 {
    public static void main(String[] args) {
        Logger logger = Logger.getLogger("src.main.java.chapter5");
        Task2 app = new Task2();
        try {
            logger.info(String.valueOf(app.sumOfValues("/Users/aleksey/Desktop/Универ/4 курс/Java/LabsMaven/src/main/java/chapter5/input.txt")));
            logger.info(String.valueOf(app.sumOfValues("")));
        } catch ( IllegalArgumentException | FileNotFoundException e) {
            logger.info(e.getMessage());
        }
    }
}
