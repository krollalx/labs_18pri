package chapter5;

import java.io.FileNotFoundException;
import java.util.ArrayList;

public class Task2 {
    public double sumOfValues(String filename) throws FileNotFoundException {
        Task1 delegate = new Task1();
        ArrayList<Double> data = delegate.readValues(filename);
        return data.stream().mapToDouble(i -> i).sum();
    }
}
