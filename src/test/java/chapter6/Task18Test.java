package chapter6;

import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

public class Task18Test {
    @Test
    public void test1() {
        Integer[] greetings = Task18.repeat(10, 42, Integer[]::new);
        Integer[] expected = {42, 42, 42, 42, 42, 42, 42, 42, 42, 42};
        assertArrayEquals(expected, greetings);
    }
}