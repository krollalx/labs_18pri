package chapter5;

import org.junit.Test;

import java.io.FileNotFoundException;

import static org.junit.jupiter.api.Assertions.*;

public class Task3Test {
    @Test
    public void test1() {
        Task2 app = new Task2();
        try {
            double sum = app.sumOfValues("/Users/aleksey/Desktop/Универ/4 курс/Java/LabsMaven/src/main/java/chapter5/input.txt");
            assertEquals(sum, 21.6);
        } catch ( IllegalArgumentException | FileNotFoundException e) {
            System.err.println(e.getMessage());
        }
    }

    @Test
    public void test2() {
        Task2 app = new Task2();
        assertThrows(FileNotFoundException.class, () -> app.sumOfValues(""));
    }

    @Test
    public void test3() {
        Task2 app = new Task2();
        assertThrows(NumberFormatException.class, () -> app.sumOfValues("/Users/aleksey/Desktop/Универ/4 курс/Java/LabsMaven/src/main/java/chapter5/test.txt"));
    }
}